
# MAIN FUNCTION TO CALL THE L1B MODULE

from l1b.src.l1b import l1b
#from lib.own_code.compare import compare

# Directory - this is the common directory for the execution of the E2E, all modules
auxdir = '/home/luss/Desktop/proyect EODP/earth-observation-mise21/auxiliary/'

indir = '/home/luss/my_shared_folder/EODP_TER_2021/EODP-TS-L1B/input/'
outdir = '/home/luss/my_shared_folder/test_output/l1b/'

 # Toulouse run E2E
# indir = '/home/luss/my_shared_folder/test_output/e2e/ism/'
# outdir = '/home/luss/my_shared_folder/test_output/e2e/l1b/'

# Initialise the ISM
myL1b = l1b(auxdir, indir, outdir)
myL1b.processModule()

# Compare Lucia with Angel results
# compare()
