# from own_code.compare.py import compare
import numpy as np
from common.io.writeToa import writeToa, readToa
from config import globalConfig
import matplotlib.pyplot as plt

gC = globalConfig.globalConfig()

dir_luss = '/home/luss/my_shared_folder/EODP_TER_2021/EODP-TS-L1B/output/'
dir_angel = '/home/luss/my_shared_folder/test_output/l1b/'
outdir = '/home/luss/my_shared_folder/test_output/l1b/'

# def compareError(x,y):
#     return np.max(np.absolute((x - y)/y)*100)
#
# def comparePlot(toa_angel , toa_luss):
#     plt.figure()
#     plt.plot(toa_angel,label = "TOA Angel")
#     plt.plot(toa_luss, label = "TOA Luss")
#     plt.title("Comparison of " + band)
#
#     plt.xlabel("ALT Pixel [-]")
#     plt.ylabel("TOA [mW/m2/sr]")
#     plt.legend()
#     #plt.show()
#     plt.savefig(outdir + "l1b_toa_eq_comparison-" + band + '.png')
#     plt.close()
#
#
#
# for band in gC.bands:
#     toa_angel = readToa(dir_angel, gC.l1b_toa_eq  + band + '.nc')[2,:]
#     toa_luss = readToa(dir_luss, gC.l1b_toa_eq  + band + '.nc')[2,:]
#
#     dif = compareError(toa_angel,toa_luss)
#
#     if dif >= 0.01:
#         print('Error in' + band + '(sigma= ' + str('%0.4f' % dif) + ')')
#     else:
#         print('Band ' + band + ' all right (sigma= ' + str('%0.4f' % dif) + ')')
#
#     comparePlot(toa_angel,toa_luss)


## L1B isrf comparison plot
isrf_dir = '/home/luss/my_shared_folder/test_output/ism/'

for band in gC.bands:

    toa_eq = readToa(dir_angel, 'l1b_toa_' + band + '.nc')
    toa_eq = toa_eq[int(toa_eq.shape[0]/2),:]
    toa_no_eq = readToa(dir_angel, 'l1b_toa_no_eq_'  + band + '.nc')[int(toa_eq.shape[0]/2),:]
    toa_isrf = readToa(isrf_dir, 'ism_toa_isrf_' + band + '.nc')[int(toa_eq.shape[0]/2),:]

    plt.plot(toa_eq,label = "TOA Eq.")
    plt.plot(toa_no_eq, label = "TOA No Eq.")
    plt.plot(toa_isrf, label = "TOA Isrf")
    plt.title("Effect of the equalization " + band)

    plt.xlabel("ACT Pixel [-]")
    plt.ylabel("TOA [mW/m2/sr]")
    plt.legend()
    #plt.show()
    plt.savefig(outdir + "l1b_toa_eq_effect-" + band + '.png')
    plt.close()






