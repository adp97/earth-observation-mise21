import numpy as np
from common.io.writeToa import writeToa, readToa
from config import globalConfig


gC = globalConfig.globalConfig()

dir_luss = '/home/luss/my_shared_folder/EODP_TER_2021/EODP-TS-L1C/output/'
dir_angel = '/home/luss/my_shared_folder/test_output/l1c/'


for band in gC.bands:
    toa_angel = readToa(dir_angel, gC.l1c_toa + band + '.nc')
    toa_luss = readToa(dir_luss, gC.l1c_toa  + band + '.nc')

    n_points = len(toa_luss)

    rel_err = np.absolute((np.sort(toa_angel) - np.sort(toa_luss))/np.sort(toa_luss))
    mean_err = np.mean(rel_err)
    std_err = np.std(rel_err)

    out_per = np.sum( np.absolute(rel_err - mean_err) > (3*std_err) ) / n_points * 100

    if out_per >= 0.01:
        print('Error in ' + band + '(sigma= ' + str('%0.4f' % out_per) + ')')
    else:
        print('Band ' + band + ' all right (sigma= ' + str('%0.4f' % out_per) + ')')
