import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
from common.io.l1cProduct import readL1c
from config import globalConfig


outdir = '/home/luss/my_shared_folder/test_output/l1c/'
gC = globalConfig.globalConfig()


for band in gC.bands:
    [toa, lat, lon] = readL1c(outdir, 'l1c_toa_'+ band + '.nc')

    plt.figure()

    ## Dotted plot
    # jet = cm.get_cmap('jet', toa.shape[0])
    toa[np.argwhere(toa<0)] = 0
    max_toa = np.max(toa)

    ## (Lucia)
    # for i in range(len(lat)):
    #     clr = jet(toa[i]/max_toa)
    #     plt.plot(lon[i],lat[i],'.', color=clr, markersize=2)

    ## Scatter
    plt.scatter(lon,lat, c=toa, s=2)
    plt.colorbar()
    plt.jet()


    ## Contourplot (gridded version, includes colorbar legend)
    # plt.tricontourf(lon,lat,toa,100, cmap='jet')
    # plt.colorbar()


    plt.title('TOA ' + band)
    plt.xlabel("Longitude [deg]")
    plt.ylabel("Latitude [deg]")
    plt.savefig(outdir + "/l1c_toa_map-" + band + '.png')
    # plt.show()
    plt.close()


# plot_xycontour(outdir)
