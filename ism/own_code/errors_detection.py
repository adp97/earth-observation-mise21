import numpy as np
from common.io.writeToa import writeToa, readToa
from config import globalConfig

def compareError(x,y):
    # Funcion Raul
    rel_error = np.absolute((x - y)/y)
    mean_error = np.mean(rel_error)
    std_error = np.std(rel_error)

    n_points = x.shape[0]*x.shape[1]

    out_per = np.sum( np.absolute(rel_error - mean_error) > (3*std_error) ) / n_points * 100

    # # Funcion mia
    # out_per = np.max(np.absolute((x - y)/y)*100)

    return out_per

gC = globalConfig.globalConfig()

dir_luss = '/home/luss/my_shared_folder/EODP_TER_2021/EODP-TS-ISM/output/'
dir_angel = '/home/luss/my_shared_folder/test_output/ism/'
type = ''

for band in gC.bands:
    toa_angel = readToa(dir_angel, 'ism_toa_' + type + band + '.nc')
    toa_luss = readToa(dir_luss, 'ism_toa_' + type + band + '.nc')

    dif = compareError(toa_angel,toa_luss)


    if dif >= 0.01:
        print('Error in ' + band + '(sigma= ' + str('%0.4f' % dif) + ')')
    else:
        print('Band ' + band + ' all right (sigma= ' + str('%0.4f' % dif) + ')')
